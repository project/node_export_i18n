# NODE_EXPORT_i18n 

## INTRODUCTION

This module extends the Node Export functionality and introduces some language localization export/import values specifically when using this module in conjunction with Entity Translations module. For whatever reason, this combination exists and with 1 node representing several translations, it means we are missing certain values on export. 

For instance:

* path aliases only have the original language (en usually)
* menus only contain the original language (en usually)
* titles -- unless you are using the title_field module, you'll be having fun with these as well.

Before looking to this module, make sure you've exhausted your use of .PO files by exporting all your necessary strings, if they are translated using that mechanism. Whatever remains, if not covered by the normalized node export, then you can enable this module to help with some brute force export/import node translation migration. It just helps to know it can all be stored and migrated through a single file.

We hope by introducing this module, we get to fix a few of those inconsistencies!

## INSTALLATION
1. Copy node_export_i18n folder to modules directory (usually sites/all/modules).
1. At admin/build/modules enable the Node export i18n module in the Node export
   package.
1. Export/import and forget!


## CONFIGURATION
* not applicable


## USAGE

1. Check out the Node Export documentation.


## ROADMAP

1. Add in the normalized title field approach. For now, we wait...
1. Add in test scripts