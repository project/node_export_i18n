<?php

/**
 * @file
 * The Node export i18n module.
 *
 * Extends the Node Export functionality and introduces some language localization export/import values specifically
 * when using this module in conjunction with Entity Translations module. For whatever reason, this combination exists
 * and with 1 node representing several translations, it means we are missing certain values. For instance:
 *
 *  - path aliases only have the original language (en usually)
 *  - menus only contain the original language (en usually)
 *  - titles -- unless you are using the title_field module, you'll be having fun with these as well.
 *
 * If you have the time, make effective use of .PO files, exporting all your necessary strings, if they are translated
 * using that mechanism. If not, you can enable this module to help with some brute force export/import node translation
 * migration. It just helps to know it can all be stored and migrated through a single file.
 */

/**
 * Prepare a single node during export.
 *
 * I've chosen to build the path and menu arrays using the language as keys as it is a common approach and best practice
 * for most entity object construction. I then normalize it back before import and do the necessary db saves after import
 * when we have proper IDs available.
 */
function node_export_i18n_node_export_node_alter(&$node, $original_node) {
  // Get all the translations languages
  $translations = $node->translations->data;
  // Get rid of the original as we won't be exporting these.
  unset($translations[$node->translations->original]);

  if (module_exists('pathauto') && module_exists('entity_translation')) {
    // @TODO: Add the title field to the mix.
    // Redefine the variables with language keys
    $node->path = array($node->translations->original => $node->path);
    $node->menu = array($node->translations->original => $node->menu);
    // Add in the other languages besides the original language
    foreach ($translations as $langcode => $language) {
      $node->path[$langcode] = path_load(array('source' => 'node/' . $node->nid, 'language' => $langcode));
      $node->menu[$langcode] = node_export_i18n_get_menu($node, $langcode);
    }
  }
}

/**
 * Get the new language menu and setup its array as is used by the export module.
 */
function node_export_i18n_get_menu($node, $language) {
  $menu = i18n_menu_link_load($node->path[$language]['source'], $language);
  $menu = array_intersect_key($menu, $node->menu[$node->translations->original]);
  $menu['parent_depth_limit'] = $node->menu[$node->translations->original]['parent_depth_limit'];
  $menu['options'] = $node->menu[$node->translations->original]['options'];
  $menu['enabled'] = $node->menu[$node->translations->original]['enabled'];
  $menu['mlid'] = $node->menu[$node->translations->original]['mlid'];
  return $menu;
}


/**
 * Implements hook_node_export_node_import_alter().
 *
 * This permits us to clean up the node object and ensure we have proper keys and arrays going into the node_save()
 */
function node_export_i18n_node_export_node_import_alter(&$node, $original_node, $save){
  // Get all the translations languages
  $translations = $node->translations->data;
  // Get rid of the original as we won't be exporting these.
  unset($translations[$node->translations->original]);

  if (module_exists('pathauto') && module_exists('entity_translation')) {
    // @TODO: Add the title field to the mix.
    // Reset the path array to normalized node style
    $node->path = $original_node->path[$node->translations->original];
    $node->menu = $original_node->menu[$node->translations->original];
    // Setup a variable for our module to use to write the paths to the DB.
    $node->node_export_i18n = new stdClass();
    foreach ($translations as $langcode => $language) {
      $node->node_export_i18n->path[$langcode] = $original_node->path[$langcode];
      $node->node_export_i18n->menu[$langcode] = $original_node->menu[$langcode];
    }
  }
}

/**
 * Implements hook_node_export_after_import_alter().
 *
 * This allows the specific elements from the previous import_alter to save the translations.
 */
function node_export_i18n_node_export_after_import_alter(&$nodes, $format, $save){
  if (module_exists('pathauto') && module_exists('entity_translation')) {

    foreach ($nodes as $i => $node) {
      // Get all the translations languages
      $translations = $node->translations->data;
      // Get rid of the original as we won't be exporting these.
      unset($translations[$node->translations->original]);

      foreach ($translations as $langcode => $language) {
        $path = $node->node_export_i18n->path[$langcode];
        $menu = $node->node_export_i18n->menu[$langcode];

        $path_return = _node_export_i18n_path_save($path, $node);
        // @TODO: Add a path_return check.
        _node_export_i18n_menu_save($menu, $node, $langcode);
      }
    }
  }
}

/**
 * Helper function to set the alias for the node in translated languages
 *
 * @TODO: Add check to see if the path already exists and update as opposed to create.
 */
function _node_export_i18n_path_save($path, $node) {
  if ($path !== NULL) {
    module_load_include('inc', 'pathauto');
    if (is_numeric($path['pid'])) {
      unset($path['pid']);
    }
    $uri = entity_uri('node', $node);
    $path['source'] = $uri['path'];
    return _pathauto_set_alias($path, NULL, 'update');
  } else {
    return FALSE;
  }
}

/**
 * Helper function to set the menu for the node in translated languages
 *
 * @TODO: Add check to see if the menu_link already exists and update as opposed to create.
 */
function _node_export_i18n_menu_save($menu, $node, $langcode) {
  if ($menu !== NULL) { // @TODO: check for menu object instead
    $uri = entity_uri('node', $node);
    $new_menu = array_merge($uri['options']['entity']->menu, $menu);
    if (function_exists('menu_link_save')) {
      if (!menu_link_save($new_menu)) {
        drupal_set_message(t('There was an error saving the menu link.'), 'error');
      }
    }
    // @TODO: Add return functionality
  }
}